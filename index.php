<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP activity</title>
</head>
<body>
		<h1>ACTIVITY 1</h1>
		<h2>FULL ADDRESS</h2>
		<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswynn Bldg."); ?></p>

		<h2>Letter-Based Grading</h2>
		<p><?php echo getLetterGrade(82); ?></p>
</body>
</html>



